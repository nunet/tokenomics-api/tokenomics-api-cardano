FROM ubuntu

# 2. Set up environment variables
ENV USERNAME=pioneer \
    USER_UID=2001 \
    USER_GID=2001 \
    DEBIAN_FRONTEND=noninteractive \
    BOOTSTRAP_HASKELL_NONINTERACTIVE=yes \
    BOOTSTRAP_HASKELL_NO_UPGRADE=yes

# 3. Update image and install pre-req packages
RUN apt-get update
RUN apt-get install -y --no-install-recommends sed curl gcc git make sudo vim xz-utils libtinfo5 libgmp-dev zlib1g-dev procps lsb-release ca-certificates build-essential libffi-dev libgmp-dev libgmp10 libncurses-dev libncurses5 libtinfo5 libicu-dev libncurses-dev z3 libnuma-dev

# 4. Create dev user
RUN groupadd --gid $USER_GID $USERNAME && \
    useradd -ms /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME && \
    echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME && \
    chmod 0440 /etc/sudoers.d/$USERNAME

# 5. Switch to dev user
USER ${USER_UID}:${USER_GID}
WORKDIR /home/${USERNAME}
ENV PATH="/home/${USERNAME}/.local/bin:/home/${USERNAME}/.cabal/bin:/home/${USERNAME}/.ghcup/bin:/home/${USERNAME}/.nix-profile/bin:$PATH"

# 6. Update dev user's PATH environment variable
RUN echo "" >> /home/${USERNAME}/.profile && \
    echo "#Updated PATH" >> /home/${USERNAME}/.profile && \
    echo "export PATH=$PATH" >> /home/${USERNAME}/.profile

#######################
# Install Haskell
#######################
RUN curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh

#######################
# Install Nix
#######################
# 1. Install Nix
RUN curl -L https://nixos.org/nix/install | sh

# 2. Add Nix script to bashrc
RUN echo ". /home/${USERNAME}/.nix-profile/etc/profile.d/nix.sh" >> /home/${USERNAME}/.bashrc

# 3. Set up caching
RUN mkdir -p /home/${USERNAME}/.config/nix && \
    touch /home/${USERNAME}/.config/nix/nix.conf && \
    printf "substituters        = https://hydra.iohk.io https://iohk.cachix.org https://cache.nixos.org/\ntrusted-public-keys = hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ= iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo= cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=" >> ~/.config/nix/nix.conf && \
    sudo mkdir /etc/nix && \
    sudo touch /etc/nix/nix.conf && \
    cat /home/${USERNAME}/.config/nix/nix.conf | sudo tee -a /etc/nix/nix.conf > /dev/null

RUN mkdir tokenomics-api

COPY . tokenomics-api

EXPOSE 9080

RUN cd tokenomics-api && \
    nix-shell

#######################
# Wrapping up
#######################
# 1. Make container interactive
ENV DEBIAN_FRONTEND=dialog

CMD [ "cabal run plutus-example-projects-test" ] 
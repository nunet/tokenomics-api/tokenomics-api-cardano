module Signing where

import Cardano.Address
import Cardano.Crypto.Seed
import Cardano.Crypto.Signing (SigningKey (..))

-- Replace "path/to/my.skey" with the path to your .skey file
readSigningKey :: FilePath -> IO (Maybe SigningKey)
readSigningKey filePath = do
    skey <- readFileText filePath
    let mskey = deserialiseSigningKey skey
    case mskey of
        Just (XPrv xprv) -> return $ Just $ SigningKey $ toVerification xprv
        _ -> return Nothing

readOracleKey :: SigningKey

readOracleKey = do
    maybeSigningKey <- readSigningKey "./oracle.skey"
    case maybeSigningKey of
        Just signingKey -> return signingKey
        Nothing -> error "Failed to read signing key"

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# DeriveAnyClass #-}

module Main where

import Cardano.Api qualified as Script
import Cardano.Api.Shelley qualified as Script
import Cardano.Crypto.Wallet as CryptoWallet
import Control.Monad hiding (fmap)
import Control.Monad.Freer.Extras as Extras
import Data.ByteString.Base16 as Base16
import Data.Default (Default (..))
import Data.Either.Unwrap
import Data.Map as Map
import Data.Monoid (Last (..))
import Ledger
import Ledger.Ada as Ada
import Ledger.CardanoWallet as CW
import Ledger.Scripts
import Ledger.TimeSlot
import Ledger.Value
import Plutus.Contract.Oracle as Oracle
import Plutus.Contract.Test (checkPredicate, walletFundsChange, (.&&.))
import Plutus.Contract.Test.Coverage.ReportCoverage (writeCoverageReport)
import Plutus.Contract.Trace as X
import Plutus.Trace.Emulator as Emulator
import Plutus.V1.Ledger.Api
import Plutus.V1.Ledger.Bytes
import Plutus.V1.Ledger.Scripts
import PlutusCore.Data
import PlutusTx.Builtins as Builtins
import PlutusTx.Builtins.Class (stringToBuiltinByteString)
import PlutusTx.Prelude
import SimpleEscrow
import System.Exit (ExitCode (..))
import Test.Tasty
import Test.Tasty.HUnit qualified as HUnit
import Wallet.Emulator.Wallet
import Prelude (IO, Show (..))
import Prelude qualified as Pr

main :: IO ()
main = return ()

test1 :: IO ()
test1 = runEmulatorTraceIO' def emCfg testForSuccessfullRedeemTokenFunding

test2 :: IO ()
test2 = runEmulatorTraceIO' def emCfg testForSuccessfullRedeemTokenWithdraw

test3 :: IO ()
test3 = runEmulatorTraceIO' def emCfg testLockandWithdraw

test4 :: IO ()
test4 = runEmulatorTraceIO' def emCfg testLockandRefund

test5 :: IO ()
test5 = runEmulatorTraceIO' def emCfg testLockAndDistributeProvider

test6 :: IO ()
test6 = runEmulatorTraceIO' def emCfg testLockAndDistributeConsumer

test7 :: IO ()
test7 = runEmulatorTraceIO' def emCfg testRedeemerToken

providerWallet, consumerWallet :: Wallet
providerWallet = X.knownWallet 1 -- user
consumerWallet = X.knownWallet 2 -- computeProvider

oracleWallet, fakeWallet :: MockWallet
oracleWallet = CW.knownMockWallet 1
fakeWallet = CW.knownMockWallet 2

emCfg :: EmulatorConfig
emCfg = EmulatorConfig (Left $ Map.fromList [(X.knownWallet w, v) | w <- [1 .. 3]]) def
  where
    v :: Value
    v = Ada.lovelaceValueOf 1000_000_000 <> assetClassValue ntxToken 1000_000_000

currency :: CurrencySymbol
currency = "eb2981df04ea2954aca086449a8a742edf25a02b239bd464669791cb"

k :: TokenName
k = "NTX"

ntxToken :: AssetClass
ntxToken = AssetClass (currency, k)

testContractAmount :: Integer
testContractAmount = 80_000_000

testContractEndTime :: POSIXTime
testContractEndTime = slotToBeginPOSIXTime def 26

buildFundParam :: BuiltinByteString -> GiveParams
buildFundParam message =
  GiveParams
    { gpBeneficiary = pkh2,
      gpOracle = pkh3,
      gpUserAddress = pkh1,
      gpRedeemActionToken = sig,
      gpMessage = message,
      gpDeadline = testContractEndTime,
      gpAmount = 10000000,
      gptokenPolicyId = "eb2981df04ea2954aca086449a8a742edf25a02b239bd464669791cb",
      gptokenName = "NTX",
      gpNTXTokenAmount = 10000000
    --   gpFundParam = fundingHash,
    --   gpWithdrawParam = withdrawHash,
    --   gpRefundParam = refundHash, 
    --   gpDistributeParam = distributeHash    
      }
  where
    pkh1 = mockWalletPaymentPubKeyHash providerWallet
    pkh2 = mockWalletPaymentPubKeyHash consumerWallet
    pkh3 = paymentPubKey $ oracleWallet

    prv = unPaymentPrivateKey $ paymentPrivateKey $ oracleWallet
    sig  = signRedeemActionToken message prv
    sigString  = getSignature (sig)
    prvFake = unPaymentPrivateKey $ paymentPrivateKey $ fakeWallet
    fundingHash = DatumHash $ stringToBuiltinByteString "fund"
    withdrawHash = DatumHash $ stringToBuiltinByteString "withdraw"
    refundHash = DatumHash $ stringToBuiltinByteString "refund"
    distributeHash = DatumHash $ stringToBuiltinByteString "distribute"

buildWithdrawParam :: BuiltinByteString ->  GrabParams
buildWithdrawParam message =
  GrabParams
    { gbtokenPolicyId = "eb2981df04ea2954aca086449a8a742edf25a02b239bd464669791cb",
      gbtokenName = "NTX",
      gbOracle = pkh3,
      gbUserAddress = pkh1,
      gbDeadline = testContractEndTime,
      gbRedeemToken =  signRedeemActionToken message prv,
      gbFundParam = fundingHash,
      gbWithdrawParam = withdrawHash,
      gbRefundParam = refundHash, 
      gbDistributeParam = distributeHash
    }
  where
    pkh1 = mockWalletPaymentPubKeyHash providerWallet
    pkh3 = paymentPubKey $ oracleWallet
    prv = unPaymentPrivateKey $ paymentPrivateKey $ oracleWallet
    fundingHash = DatumHash $ stringToBuiltinByteString "fund"
    withdrawHash = DatumHash $ stringToBuiltinByteString "withdraw"
    refundHash = DatumHash $ stringToBuiltinByteString "refund"
    distributeHash = DatumHash $ stringToBuiltinByteString "distribute"

testForSuccessfullRedeemTokenFunding :: EmulatorTrace ()
testForSuccessfullRedeemTokenFunding = do
  providerStartHdl <- activateContractWallet providerWallet endpoints
  let message = stringToBuiltinByteString "fund"
  let param = buildFundParam message
  void $ Emulator.waitNSlots 1
  callEndpoint @"start" providerStartHdl param
  void $ Emulator.waitNSlots 1

testForSuccessfullRedeemTokenWithdraw :: EmulatorTrace ()
testForSuccessfullRedeemTokenWithdraw = do
  consumerHdl <- activateContractWallet consumerWallet endpoints
  let message = stringToBuiltinByteString "ok"
  let param = buildWithdrawParam message
  void $ Emulator.waitNSlots 1
  callEndpoint @"withdraw" consumerHdl param
  void $ Emulator.waitNSlots 1

testLockandWithdraw :: EmulatorTrace ()
testLockandWithdraw = do
  providerStartHdl <- activateContractWallet providerWallet endpoints
  consumerHdl <- activateContractWallet consumerWallet endpoints

  let fundMessage = stringToBuiltinByteString "fund"
  let withdrawMessage = stringToBuiltinByteString "withdraw"


  let fundParam = buildFundParam fundMessage
  let withdrawParam = buildWithdrawParam withdrawMessage

  void $ Emulator.waitNSlots 1
  callEndpoint @"start" providerStartHdl fundParam
  void $ Emulator.waitNSlots 1

  -- 26 slots

  void $ Emulator.waitNSlots 30
  callEndpoint @"withdraw" consumerHdl withdrawParam
  void $ Emulator.waitNSlots 1

testLockandRefund :: EmulatorTrace ()
testLockandRefund = do
  providerStartHdl <- activateContractWallet providerWallet endpoints
  consumerHdl <- activateContractWallet consumerWallet endpoints

  let fundMessage = stringToBuiltinByteString "fund"
  let refundMessage = stringToBuiltinByteString "refund"


  void $ Emulator.waitNSlots 1
  callEndpoint @"start" providerStartHdl (buildFundParam fundMessage)
  void $ Emulator.waitNSlots 1

  -- 26 slots

  void $ Emulator.waitNSlots 30
  callEndpoint @"refund" providerStartHdl (buildFundParam refundMessage)
  void $ Emulator.waitNSlots 1

testLockAndDistributeProvider :: EmulatorTrace ()
testLockAndDistributeProvider = do
  providerStartHdl <- activateContractWallet providerWallet endpoints
  consumerHdl <- activateContractWallet consumerWallet endpoints

  let fundMessage = stringToBuiltinByteString "fund"
  let distributeMessage = stringToBuiltinByteString "Distribute"

  void $ Emulator.waitNSlots 1
  callEndpoint @"start" providerStartHdl (buildFundParam fundMessage)
  void $ Emulator.waitNSlots 1

  -- 26 slots

  void $ Emulator.waitNSlots 30
  callEndpoint @"distribute" providerStartHdl (buildFundParam distributeMessage)
  void $ Emulator.waitNSlots 1

testLockAndDistributeConsumer :: EmulatorTrace ()
testLockAndDistributeConsumer = do
  providerStartHdl <- activateContractWallet providerWallet endpoints
  consumerHdl <- activateContractWallet consumerWallet endpoints

  let fundMessage = stringToBuiltinByteString "fund"
  let distributeMessage = stringToBuiltinByteString "Distribute"

  void $ Emulator.waitNSlots 1
  callEndpoint @"start" providerStartHdl (buildFundParam fundMessage)
  void $ Emulator.waitNSlots 1

  -- 26 slots

  void $ Emulator.waitNSlots 30
  callEndpoint @"distribute" consumerHdl (buildFundParam distributeMessage)
  void $ Emulator.waitNSlots 1  

signRedeemActionToken :: BuiltinByteString -> PrivateKey -> Signature
signRedeemActionToken message privateKey = sign' message privateKey

data TestParams = TestParams {userAddress :: !PaymentPubKeyHash, testDeadline :: !POSIXTime, price :: !Integer} deriving ToData

testRedeemerToken :: EmulatorTrace ()
testRedeemerToken = do
  let privateKey = unPaymentPrivateKey $ paymentPrivateKey $ oracleWallet
  let messageProvider = stringToBuiltinByteString "a2c20c77887ace1cd986193e4e75babd8993cfd56995cd5cfce609c21596059117000100000"

  let messageConsumer = stringToBuiltinByteString "80a4f45b56b88d1139da23bc4c3c75ec6d32943c087f250b86193ca71596059117000100000"
  
  
  let deadlineToString = stringToBuiltinByteString $ show $ getPOSIXTime $ testContractEndTime

  let price = stringToBuiltinByteString $ show $ testContractAmount

  let encodedString = encodeUtf8 (appendString (appendString  (decodeUtf8  messageConsumer) (decodeUtf8 deadlineToString)) (decodeUtf8 price))

  let sigToken = signRedeemActionToken encodedString privateKey
  let sigDeadline = signRedeemActionToken deadlineToString privateKey
  let sigPrice = signRedeemActionToken price privateKey

  let sg = signMessage' messageProvider privateKey


  -- let dat = PlutusCore.Data $ tp
  -- let hash = Script.hashScriptData $ Script.fromPlutusData $ toData $ tp

  -- logInfo @Pr.String $ "signed Bits " <> (Pr.show $ sig)
  logInfo
    @Pr.String
    $ "signed Bits " <> (Pr.show $ sigPrice)

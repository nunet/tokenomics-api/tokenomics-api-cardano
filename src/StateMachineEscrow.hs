{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE NumericUnderscores    #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module StateMachineEscrow where

import           Control.Monad                hiding (fmap)
import           Data.Aeson                   (FromJSON, ToJSON)
import           Data.Monoid                  (Last (..))
import           Data.Text                    (Text, pack)
import           GHC.Generics                 (Generic)
import           Ledger                       hiding (singleton)
import           Ledger.Ada                   as Ada
import           Ledger.Value                   as Value

import           Ledger.Constraints           as Constraints
import           Ledger.Constraints.TxConstraints as TxConstraints
import           Ledger.Typed.Tx
import qualified Ledger.Typed.Scripts         as Scripts
import qualified PlutusTx.Builtins            as Builtins
import           Plutus.Contract              as Contract
import           Plutus.Contract.StateMachine
import qualified PlutusTx
import           PlutusTx.Prelude             hiding (Semigroup(..), check, unless)
import qualified PlutusTx.Prelude             as PP
import           Playground.Contract          (ToSchema,ensureKnownCurrencies,printSchemas)
import           Prelude                      (Semigroup (..), Show (..), String)
import qualified Prelude







data EscrowMetaData = EscrowMetaData
    { provider        :: PaymentPubKeyHash
    , consumer        :: PaymentPubKeyHash
    , oracle             :: !PaymentPubKeyHash
    , redeemToken     :: BuiltinByteString
    , startTime       :: POSIXTime
    , endTime         :: POSIXTime
    , contractValue   :: Integer
    , utxoIdentifer   :: ThreadToken
    , ntx             :: !AssetClass
    } deriving (Show, Generic, FromJSON, ToJSON, Prelude.Eq)

PlutusTx.makeLift ''EscrowMetaData

data EscrowRedeemer =
          Accept 
        | Withdraw
        | Refund 
        | Dispute 
        | Blocked
        | Unblock
        | Close
        deriving (Show, Generic, FromJSON, ToJSON, Prelude.Eq)

{-Is there a better way to do makeIsDataIndexed ?-}
PlutusTx.makeLift ''EscrowRedeemer
PlutusTx.makeIsDataIndexed ''EscrowRedeemer [('Accept, 0), ('Refund, 1), ('Withdraw, 2), ('Dispute, 3),('Blocked, 4), ('Unblock, 5), ('Close, 6)]

data EscrowDatum = Deposited | Active | Withdrawn | Disputed | Closed
    deriving (Show)

instance Eq EscrowDatum where
  (==) Deposited Deposited = True
  (==) Active Active = True
  (==) Disputed Disputed = True
  (==) _ _ = False

PlutusTx.makeIsDataIndexed ''EscrowDatum [('Deposited, 0), ('Active, 1), ('Withdrawn, 2), ('Disputed, 3),('Closed, 4)]

newtype ReedemToken = ReedemToken BuiltinByteString deriving  (PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)

PlutusTx.makeLift ''ReedemToken

newtype RefundToken = RefundToken BuiltinByteString deriving  (PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)

PlutusTx.makeLift ''RefundToken

newtype DisputeToken = DisputeToken BuiltinByteString deriving  (PlutusTx.ToData, PlutusTx.FromData, PlutusTx.UnsafeFromData)

PlutusTx.makeLift ''DisputeToken


{-# INLINABLE lovelaces #-}
lovelaces :: Value -> Integer
lovelaces = Ada.getLovelace . Ada.fromValue


verifyFundToken:: BuiltinByteString -> Bool

verifyFundToken token =  token == "Fund"

verifyWithdrawToken:: BuiltinByteString -> Bool

verifyWithdrawToken token = token == "Withdraw"

verifyRefundToken:: BuiltinByteString -> Bool

verifyRefundToken token =  token == "Refund"

verifyDisputeToken:: BuiltinByteString -> Bool

verifyDisputeToken token = token == "Dispute"


{-# INLINABLE transition #-}
transition ::  EscrowMetaData -> State EscrowDatum -> EscrowRedeemer -> Maybe (TxConstraints Void Void, State EscrowDatum)
transition  escrow s r = case ( escrow, stateData s,stateValue s, r) of
    (e, _, v, Accept)
        | verifyFundToken (redeemToken e)   -> Just ( Constraints.mustBeSignedBy (consumer escrow) <>
                                                      Constraints.mustValidateIn (to $ startTime escrow)
                                                    , State Active $
                                                    v <>
                                                    assetClassValue (ntx e) (contractValue e)
                                               )
    -- (e, Active, v, Withdraw)
    --     | verifyWithdrawToken (redeemToken e)      -> Just ( Constraints.mustBeSignedBy (provider e) <>
    --                                             Constraints.mustValidateIn (from $ endTime e v) <>
    --                                             Constraints.mustPayToPubKey (provider e) (contractValue e)
    --                                           , State Active $
    --                                             v <>
    --                                              assetClassValue (ntx e)  (contractValue e)
    --                                           )

    -- (e, Active, v, Close)             -> Just ( TxConstraints.mustSatisfyAnyOf
    --                                               [Constraints.mustBeSignedBy (provider e), Constraints.mustBeSignedBy (consumer e)] <>
    --                                             Constraints.mustValidateIn (from $ endTime escrow) <>
    --                                             Constraints.mustPayToPubKey (consumer e) (v)
    --                                           , State Closed $
    --                                             mempty
    --                                           )
    -- (e, Disputed, v, Close)           -> Just ( (TxConstraints.mustSatisfyAnyOf
    --                                               [Constraints.mustBeSignedBy (provider e), Constraints.mustBeSignedBy (consumer e)]) <>
    --                                             Constraints.mustValidateIn (from $ endTime e) <>
    --                                             Constraints.mustPayToPubKey (consumer e) (v)
    --                                           , State Closed $
    --                                             mempty
    --                                           )
    _                                 -> Nothing





-- {-# INLINABLE trancheValue #-}
-- trancheValue :: EscrowMetaData -> Value
-- trancheValue e =
--   lovelaceValueOf $ PP.divide (lovelaces $ contractValue e) (tranches e)

-- {-# INLINABLE payableTime #-}
-- payableTime :: EscrowMetaData -> Value -> POSIXTime
-- payableTime e v =
--   let start = getPOSIXTime $ startTime e; end = getPOSIXTime $ endTime e; totalTime = end - start; totalAmount = lovelaces $ contractValue e; currentAmount = lovelaces v
--     in POSIXTime $ end - (Builtins.multiplyInteger totalTime currentAmount) `PP.divide` totalAmount

-- {-# INLINABLE maxSettlementTime #-}
-- maxSettlementTime :: EscrowMetaData -> POSIXTime
-- maxSettlementTime e =
--   let end = getPOSIXTime $ endTime e
--     in POSIXTime $ end + threeDaysTime

{-# INLINABLE threeDaysTime #-}
threeDaysTime :: Integer
threeDaysTime = 100_000 -- scaling down : hack for testing. Actual is 259_200_000

{-# INLINABLE isClosed #-}
isClosed :: EscrowDatum -> Bool
isClosed Closed = True
isClosed _ = False

{-# INLINABLE escrowStateMachine #-}
escrowStateMachine ::  EscrowMetaData -> StateMachine EscrowDatum EscrowRedeemer
escrowStateMachine e = mkStateMachine (Just $ utxoIdentifer e) (transition  e) (isClosed)

{-# INLINABLE mkEscrowValidator #-}
mkEscrowValidator :: EscrowMetaData -> EscrowDatum -> EscrowRedeemer -> ScriptContext -> Bool
mkEscrowValidator  escrow = mkValidator $ escrowStateMachine  escrow

type EscrowSMType = StateMachine EscrowDatum EscrowRedeemer

escrowTypedValidator ::  EscrowMetaData -> Scripts.TypedValidator EscrowSMType
escrowTypedValidator  escrow = Scripts.mkTypedValidator @EscrowSMType
    ($$(PlutusTx.compile [|| mkEscrowValidator ||])
        `PlutusTx.applyCode` PlutusTx.liftCode escrow)
    $$(PlutusTx.compile [|| wrap ||])
  where
    wrap = Scripts.mkUntypedValidator @EscrowDatum @EscrowRedeemer

escrowValidator :: EscrowMetaData -> Validator
escrowValidator  e= Scripts.validatorScript $ escrowTypedValidator  e

escrowAddress :: EscrowMetaData -> Ledger.Address
escrowAddress  e = scriptAddress $ escrowValidator  e

escrowClient ::  EscrowMetaData -> StateMachineClient EscrowDatum EscrowRedeemer
escrowClient  e = mkStateMachineClient $ StateMachineInstance (escrowStateMachine e) (escrowTypedValidator  e)

mapError' :: Contract w s SMContractError a -> Contract w s Text a
mapError' = mapError $ pack . show


data FundParam = FundParam
    { providerAddress      :: !PaymentPubKeyHash
    , consumerAddress      :: !PaymentPubKeyHash
    , oracleAddress        :: !PaymentPubKeyHash
    , redeemTokenString    :: !BuiltinByteString
    , startTimeForFund     :: !POSIXTime
    , endTimeForFund       :: !POSIXTime
    , ntxValue             :: !Integer
    , ntxToken             :: !AssetClass
    , utxoId               :: !ThreadToken
    } deriving (Show, Generic, FromJSON, ToJSON, ToSchema)


data UseParam = UseParam
    { uproviderAddress   :: !PaymentPubKeyHash
    , uconsumerAddress   :: !PaymentPubKeyHash
    , uoracleAddress   :: !PaymentPubKeyHash
    , uredeemToken  :: !BuiltinByteString
    , ustartTime  :: !POSIXTime
    , uendTime  :: !POSIXTime
    , untxValue  :: !Integer
    , untxToken  :: !AssetClass
    , uutxoId :: !ThreadToken
    } deriving (Show, Generic, FromJSON, ToJSON, ToSchema)

-- useParamToAppliedEscrow :: UseParam -> EscrowMetaData
-- useParamToAppliedEscrow params =
--               EscrowMetaData
--                  { provider         = uproviderAddress params
--                   , consumer        = uconsumerAddress params
--                   , oracle          = uoracleAddress params
--                   , redeemToken     = uredeemToken params
--                   , startTime       = ustartTime params
--                   , endTime         = uendTime params
--                   , contractValue   = untxValue params
--                   , utxoIdentifer   = uutxoId params
--                   , ntx             = ntxToken params
--                   }


startEscrow :: forall s. FundParam -> Contract (Last ThreadToken) s Text () 

startEscrow params = do 
  pkh <- Contract.ownPaymentPubKeyHash
  tt  <- mapError' getThreadToken
  logInfo @String $ "Logging thread token: " ++ show tt
  let escrow   = EscrowMetaData
          { provider        = providerAddress params
          , consumer        = pkh
          , redeemToken     = redeemTokenString params
          , startTime       = startTimeForFund params
          , endTime         = endTimeForFund params
          , contractValue   = ntxValue params
          , utxoIdentifer   = tt
          , oracle = oracleAddress params
          , ntx = ntxToken params
          }
   
      client = escrowClient  escrow
  void $ mapError' $ runInitialise client (Deposited) mempty
  tell $ Last $ Just tt
  logInfo @String $ "Escrow contract published: " ++ show escrow


type StartAppliedEscrowSchema = Endpoint "startEscrow" FundParam


-- type UseAppliedEscrowSchema = Endpoint "accept" UseParam
                          -- .\/ Endpoint "refund" UseParam
                          -- .\/ Endpoint "dispute" UseParam
                          -- .\/ Endpoint "unblock" UseParam
                          -- .\/ Endpoint "close" UseParam

startEscrowEndpoint :: Contract (Last ThreadToken) StartAppliedEscrowSchema Text ()
startEscrowEndpoint = forever
                        $ handleError logError
                        $ awaitPromise
                        $ endpoint @"startEscrow" (\x -> startEscrow x)


-- useEscrowEndpoints :: Contract (Last ThreadToken) UseAppliedEscrowSchema Text ()
-- useEscrowEndpoints = forever
--                         $ handleError logError
--                         $ awaitPromise
--                         $ accept' -- `select` dispute' `select` unblock' `select` close'
--                         where
--                           accept'   = endpoint @"accept" (\x -> accept x)
--                           -- dispute'  = endpoint @"dispute" (\x -> dispute x)
--                           -- unblock'  = endpoint @"unblock" (\x -> unblock x)
--                           -- close'    = endpoint @"close" (\x -> close x)

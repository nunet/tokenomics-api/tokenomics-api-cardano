{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module SimpleEscrow where

import Control.Monad hiding (fmap)
import Data.Aeson (FromJSON, ToJSON)
import Data.Map as Map
import Data.Text (Text)
import Data.Void (Void)
import GHC.Generics (Generic)
import Ledger hiding (singleton)
import Ledger.Ada as Ada
import Ledger.Constraints (TxConstraints)
import Plutus.V1.Ledger.Api

import qualified Ledger.Constraints as Constraints
import qualified Ledger.Typed.Scripts as Scripts
import Ledger.Value as Value
import Plutus.Contract
import Plutus.Contract.Oracle (checkSignature)
import qualified PlutusTx
import PlutusTx.Prelude hiding (unless)
import Schema (ToSchema)
import Text.Printf (printf)
import Prelude (IO)
import qualified Prelude as Pr

data EscrowDatum = EscrowDatum
  { user :: !PaymentPubKeyHash,
    computeProvider :: !PaymentPubKeyHash,
    redeemActionToken :: !Signature,
    message :: !BuiltinByteString,
    deadline :: !POSIXTime,
    adaAmount :: !Integer,
    ntxAmount :: !Integer
  }
  deriving (Pr.Show)

-- This needs to be refactored to makeIsDataIndex for production
PlutusTx.unstableMakeIsData ''EscrowDatum

data EscrowRedeemer = Fund | Withdraw | Refund | Distribute
  deriving (Pr.Show)

PlutusTx.makeIsDataIndexed ''EscrowRedeemer [('Fund, 0), ('Withdraw, 1), ('Refund, 2), ('Distribute, 3)]
PlutusTx.makeLift ''EscrowRedeemer

data EscrowParam = EscrowParam
  { ntxTokenPolicyId :: !CurrencySymbol,
    ntxTokenName :: !TokenName,
    oracle :: !PaymentPubKey
    -- , fundParam            :: !DatumHash
    -- , withdrawParam        :: !DatumHash
    -- , refundParam          :: !DatumHash
    -- , distributeParam      :: !DatumHash
  }
  deriving (Pr.Eq, Pr.Ord, Pr.Show, Generic, ToJSON, FromJSON)

PlutusTx.makeLift ''EscrowParam

{-# INLINEABLE mkValidator #-}
-- This should validate if either user has signed the transaction and the current slot is before or at the deadline
-- or if computeProvider has signed the transaction and the deadline has passed.
mkValidator :: EscrowParam -> EscrowDatum -> EscrowRedeemer -> ScriptContext -> Bool
mkValidator ep dat redeemer ctx =
  case redeemer of
    Withdraw ->
      traceIfFalse "Needs to be signed by computeProvider" signedByComputeProvider
        && traceIfFalse "Deadline not passed" afterDeadline
        && traceIfFalse "Could not verify" verifyToken
    Distribute ->
      traceIfFalse "Needs to be signed by computeProvider or signedByComputeProvider" signedByUser || signedByComputeProvider
        && traceIfFalse "Deadline not passed" afterDeadline
        && traceIfFalse "Could not verify" verifyToken
    Refund ->
      traceIfFalse "Needs to be signed by computeProvider" signedByUser
        && traceIfFalse "Deadline not passed" afterDeadline
        && traceIfFalse "Could not verify" verifyToken
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    bCursym :: CurrencySymbol
    bCursym = ntxTokenPolicyId ep

    bTokenN :: TokenName
    bTokenN = ntxTokenName ep

    signedByUser :: Bool
    signedByUser = (txSignedBy info $ unPaymentPubKeyHash $ user dat)

    signedByComputeProvider :: Bool
    signedByComputeProvider = txSignedBy info $ unPaymentPubKeyHash $ computeProvider dat

    beforeDeadline :: Bool
    beforeDeadline = contains (to $ deadline dat) $ txInfoValidRange info

    afterDeadline :: Bool
    afterDeadline = contains (from $ 1 + deadline dat) $ txInfoValidRange info

    -- verifyFundToken :: Bool
    -- verifyFundToken = verifyToken (fundParam ep)

    -- verifyRefundToken :: Bool
    -- verifyRefundToken = verifyToken (refundParam ep)

    -- verifyWithdrawToken :: Bool
    -- verifyWithdrawToken = verifyToken (withdrawParam ep)

    -- verifyDistributeToken :: Bool
    -- verifyDistributeToken = verifyToken (distributeParam ep)

    verifyToken :: Bool
    verifyToken = do
      case checkSignature (DatumHash $ message dat) (oracle ep) (redeemActionToken dat) of
        Right () -> True
        Left err -> False

data Escrow

instance Scripts.ValidatorTypes Escrow where
  type DatumType Escrow = EscrowDatum
  type RedeemerType Escrow = EscrowRedeemer

typedValidator :: EscrowParam -> Scripts.TypedValidator Escrow
typedValidator ep =
  Scripts.mkTypedValidator @Escrow
    ($$(PlutusTx.compile [||mkValidator||]) `PlutusTx.applyCode` PlutusTx.liftCode ep)
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = Scripts.mkUntypedValidator @EscrowDatum @EscrowRedeemer

validator :: EscrowParam -> Scripts.Validator
validator = Scripts.validatorScript . typedValidator

scrAddress :: EscrowParam -> Ledger.Address
scrAddress = scriptAddress . validator

data GiveParams = GiveParams
  { gpBeneficiary :: !PaymentPubKeyHash,
    gpOracle :: !PaymentPubKey,
    gpUserAddress :: !PaymentPubKeyHash,
    gpDeadline :: !POSIXTime,
    gpRedeemActionToken :: !Signature,
    gpMessage :: !BuiltinByteString,
    gpAmount :: !Integer,
    gptokenPolicyId :: !CurrencySymbol,
    gptokenName :: !TokenName,
    gpNTXTokenAmount :: !Integer
    -- , gpFundParam         :: !DatumHash
    -- , gpRefundParam       :: !DatumHash
    -- , gpDistributeParam   :: !DatumHash
    -- , gpWithdrawParam     :: !DatumHash
  }
  deriving (Generic, ToJSON, FromJSON, ToSchema)

PlutusTx.unstableMakeIsData ''GiveParams

type EscrowSchema =
  Endpoint "start" GiveParams
    .\/ Endpoint "withdraw" GrabParams
    .\/ Endpoint "refund" GiveParams
    .\/ Endpoint "distribute" GiveParams

start :: AsContractError e => GiveParams -> Contract w s e ()
start gp = do
  pkh <- ownPaymentPubKeyHash
  logInfo @Pr.String $
    printf
      "Own Payment Pub key %s"
      (Pr.show $ pkh)
  let ep =
        EscrowParam
          { ntxTokenPolicyId = gptokenPolicyId gp,
            ntxTokenName = gptokenName gp,
            oracle = gpOracle gp
            -- ,   fundParam = gpFundParam gp
            -- ,   withdrawParam = gpWithdrawParam gp
            -- ,   refundParam = gpRefundParam gp
            -- ,   distributeParam = gpDistributeParam gp
          }
  ownUtxos <- utxosAt (scrAddress ep)

  let dat =
        EscrowDatum
          { user = pkh,
            computeProvider = gpBeneficiary gp,
            redeemActionToken = gpRedeemActionToken gp,
            adaAmount = gpAmount gp,
            message = gpMessage gp,
            ntxAmount = gpNTXTokenAmount gp,
            deadline = gpDeadline gp
          }
      orefs = fst <$> Map.toList ownUtxos
      ntxVal = Value.singleton (gptokenPolicyId gp) (gptokenName gp) (gpNTXTokenAmount gp)

      lookups =
        Constraints.unspentOutputs ownUtxos
          Pr.<> Constraints.typedValidatorLookups (typedValidator ep)

      tx = Constraints.mustPayToTheScript dat (ntxVal <> Ada.lovelaceValueOf (gpAmount gp))

  logInfo @Pr.String $ "params " Pr.<> (Pr.show $ dat)
  ledgerTx <- submitTxConstraintsWith @Escrow lookups tx
  void $ awaitTxConfirmed $ getCardanoTxId ledgerTx

  logInfo @Pr.String $
    printf
      "locked of %d ntx to %s with deadline %s"
      (gpAmount gp)
      (Pr.show $ gpBeneficiary gp)
      (Pr.show $ gpDeadline gp)

data GrabParams = GrabParams
  { gbtokenPolicyId :: !CurrencySymbol,
    gbtokenName :: !TokenName,
    gbOracle :: !PaymentPubKey,
    gbUserAddress :: !PaymentPubKeyHash,
    gbDeadline :: !POSIXTime,
    gbRedeemToken :: !Signature,
    gbFundParam :: !DatumHash,
    gbRefundParam :: !DatumHash,
    gbDistributeParam :: !DatumHash,
    gbWithdrawParam :: !DatumHash
  }
  deriving (Generic, ToJSON, FromJSON)

PlutusTx.unstableMakeIsData ''GrabParams

withdraw :: forall w s e. AsContractError e => GrabParams -> Contract w s e ()
withdraw gb = do
  now <- currentTime
  pkh <- ownPaymentPubKeyHash

  let ep =
        EscrowParam
          { ntxTokenPolicyId = gbtokenPolicyId gb,
            ntxTokenName = gbtokenName gb,
            oracle = gbOracle gb
            -- ,   fundParam = gbFundParam gb
            -- ,   withdrawParam = gbWithdrawParam gb
            -- ,   refundParam = gbRefundParam gb
            -- ,   distributeParam = gbDistributeParam gb
          }

  logInfo @Pr.String $ "Hash " <> (Pr.show $ gbWithdrawParam gb)

  logInfo @Pr.String $ "Sig " <> (Pr.show $ (gbRedeemToken gb))

  utxos <- Map.filter (isSuitable (gbUserAddress gb) pkh now) <$> utxosAt (scrAddress ep)

  ownUtxos <- utxosAt (scrAddress ep)

  let rEscrow = Redeemer $ PlutusTx.toBuiltinData Withdraw

  if Map.null utxos
    then logInfo @Pr.String $ "no such utxo"
    else do
      let orefs = fst <$> Map.toList utxos
          lookups =
            Constraints.unspentOutputs utxos
              Pr.<> Constraints.otherScript (validator ep)
          tx =
            mconcat [Constraints.mustSpendScriptOutput oref rEscrow | oref <- orefs]
              <> Constraints.mustValidateIn (from now)
      ledgerTx <- submitTxConstraintsWith @Escrow lookups tx
      void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
      logInfo @Pr.String $ "collected payment"
  where
    isSuitable :: PaymentPubKeyHash -> PaymentPubKeyHash -> POSIXTime -> ChainIndexTxOut -> Bool
    isSuitable pkhUser pkhComputerProvider now o = case _ciTxOutDatum o of
      Left _ -> False
      Right (Datum e) -> case PlutusTx.fromBuiltinData e of
        Nothing -> False
        Just d -> user d == pkhUser && computeProvider d == pkhComputerProvider && deadline d <= now

refund :: forall w s e. AsContractError e => GiveParams -> Contract w s e ()
refund gp = do
  now <- currentTime
  pkh <- ownPaymentPubKeyHash

  let ep =
        EscrowParam
          { ntxTokenPolicyId = gptokenPolicyId gp,
            ntxTokenName = gptokenName gp,
            oracle = gpOracle gp
            -- ,   fundParam = gpFundParam gp
            -- ,   withdrawParam = gpWithdrawParam gp
            -- ,   refundParam = gpRefundParam gp
            -- ,   distributeParam = gpDistributeParam gp
          }

  let dat =
        EscrowDatum
          { user = gpUserAddress gp,
            computeProvider = gpBeneficiary gp,
            redeemActionToken = gpRedeemActionToken gp,
            message = gpMessage gp,
            adaAmount = gpAmount gp,
            ntxAmount = gpNTXTokenAmount gp,
            deadline = gpDeadline gp
          }

  logInfo @Pr.String $ "User address " <> (Pr.show $ gpUserAddress gp)

  logInfo @Pr.String $ "Ur address " <> (Pr.show $ pkh)

  logInfo @Pr.String $ "time " <> (Pr.show $ now)

  utxos <- Map.filter (isSuitable (gpUserAddress gp) (gpBeneficiary gp) now) <$> utxosAt (scrAddress ep)

  ownUtxos <- utxosAt (scrAddress ep)

  logInfo @Pr.String $ "UTXOS" <> (Pr.show $ utxos)

  logInfo @Pr.String $ printf "utxos at " <> (Pr.show $ ownUtxos)

  let rEscrow = Redeemer $ PlutusTx.toBuiltinData Refund

  if Map.null utxos
    then logInfo @Pr.String $ "no such utxo"
    else do
      let orefs = fst <$> Map.toList utxos
          lookups =
            Constraints.unspentOutputs utxos
              Pr.<> Constraints.otherScript (validator ep)
          tx =
            mconcat [Constraints.mustSpendScriptOutput oref rEscrow | oref <- orefs]
              <> Constraints.mustValidateIn (from now)
      ledgerTx <- submitTxConstraintsWith @Escrow lookups tx
      void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
      logInfo @Pr.String $ "collected payment"
  where
    isSuitable :: PaymentPubKeyHash -> PaymentPubKeyHash -> POSIXTime -> ChainIndexTxOut -> Bool
    isSuitable pkhUser pkhComputerProvider now o = case _ciTxOutDatum o of
      Left _ -> False
      Right (Datum e) -> case PlutusTx.fromBuiltinData e of
        Nothing -> False
        Just d -> user d == pkhUser && computeProvider d == pkhComputerProvider && deadline d <= now

distribute :: forall w s e. AsContractError e => GiveParams -> Contract w s e ()
distribute gp = do
  now <- currentTime
  pkh <- ownPaymentPubKeyHash

  let ep =
        EscrowParam
          { ntxTokenPolicyId = gptokenPolicyId gp,
            ntxTokenName = gptokenName gp,
            oracle = gpOracle gp
            -- ,   fundParam = gpFundParam gp
            -- ,   withdrawParam = gpWithdrawParam gp
            -- ,   refundParam = gpRefundParam gp
            -- ,   distributeParam = gpDistributeParam gp
          }

  let dat =
        EscrowDatum
          { user = gpUserAddress gp,
            computeProvider = gpBeneficiary gp,
            redeemActionToken = gpRedeemActionToken gp,
            message = gpMessage gp,
            adaAmount = gpAmount gp,
            ntxAmount = gpNTXTokenAmount gp,
            deadline = gpDeadline gp
          }

  logInfo @Pr.String $ "time " <> (Pr.show $ now)

  utxos <- Map.filter (isSuitable (gpUserAddress gp) (gpBeneficiary gp) now) <$> utxosAt (scrAddress ep)

  ownUtxos <- utxosAt (scrAddress ep)

  logInfo @Pr.String $ "UTXOS" <> (Pr.show $ utxos)

  logInfo @Pr.String $ printf "utxos at " <> (Pr.show $ ownUtxos)

  let rEscrow = Redeemer $ PlutusTx.toBuiltinData Distribute

  if Map.null utxos
    then logInfo @Pr.String $ "no such utxo"
    else do
      let halfNTX = PlutusTx.Prelude.divide (gpNTXTokenAmount gp) 2
          halfAda = PlutusTx.Prelude.divide (gpAmount gp) 2
          halfNTXVal = Value.singleton (gptokenPolicyId gp) (gptokenName gp) (halfNTX)
          otherHalfAda = (gpAmount gp) - halfAda
          otherHalfNTXVal = Value.singleton (gptokenPolicyId gp) (gptokenName gp) ((gpNTXTokenAmount gp) - halfNTX)
      let orefs = fst <$> Map.toList utxos
          lookups =
            Constraints.unspentOutputs utxos
              Pr.<> Constraints.otherScript (validator ep)
          tx =
            mconcat [Constraints.mustSpendScriptOutput oref rEscrow | oref <- orefs]
              <> Constraints.mustValidateIn (from now)
              <> Constraints.mustPayToPubKey (gpUserAddress gp) (halfNTXVal <> Ada.lovelaceValueOf halfAda)
              <> Constraints.mustPayToPubKey (gpBeneficiary gp) (otherHalfNTXVal <> Ada.lovelaceValueOf otherHalfAda)
      ledgerTx <- submitTxConstraintsWith @Escrow lookups tx
      void $ awaitTxConfirmed $ getCardanoTxId ledgerTx
      logInfo @Pr.String $ "collected payment"
  where
    isSuitable :: PaymentPubKeyHash -> PaymentPubKeyHash -> POSIXTime -> ChainIndexTxOut -> Bool
    isSuitable pkhUser pkhComputerProvider now o = case _ciTxOutDatum o of
      Left _ -> False
      Right (Datum e) -> case PlutusTx.fromBuiltinData e of
        Nothing -> False
        Just d -> user d == pkhUser && computeProvider d == pkhComputerProvider && deadline d <= now

endpoints :: Contract () EscrowSchema Text ()
endpoints = awaitPromise (start' `select` withdraw' `select` refund' `select` distribute') >> endpoints
  where
    start' = endpoint @"start" start
    withdraw' = endpoint @"withdraw" withdraw
    refund' = endpoint @"refund" refund
    distribute' = endpoint @"distribute" distribute

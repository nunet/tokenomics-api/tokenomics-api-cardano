{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module WalletServer where

import Web.Scotty
import Network.HTTP.Types.Status (statusCode, badRequest400)
import Data.Aeson (FromJSON, ToJSON, withObject, (.:), (.=), object, decode, encode,eitherDecode)
import Data.Aeson.Types (parseJSON, toJSON) -- Add this line
import Data.Text.Lazy.Encoding (decodeUtf8, encodeUtf8)
import Control.Monad.IO.Class (liftIO)
import Data.Text.Lazy (Text)
import Wallet 
import PlutusTx.Builtins 
import qualified Data.ByteString.Char8 as BSC
import PlutusTx.Builtins.Class (stringToBuiltinByteString)
import Data.ByteString (ByteString, pack)
import System.Random (randomRIO)
import PlutusTx (toBuiltinData)
import PlutusTx.Prelude (BuiltinByteString)
import Ledger.Crypto 
import Plutus.Contract.Oracle 
import Ledger 
import Plutus.V1.Ledger.Time as TimeV1
import Plutus.V1.Ledger.Api 
import Control.Monad (replicateM)
import Data.String (IsString, fromString)
import PlutusTx.IsData.Class
import Control.Monad.Logger 
import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import Data.Text.IO as TIO
import Data.ByteString.Builder (toLazyByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Text.Lazy.Encoding (decodeUtf8)
import System.Log.FastLogger (LogStr, fromLogStr)
import qualified Data.Text as T
import Data.Aeson (encode)
import Data.Text.Encoding (decodeUtf8')
import Data.Either.Combinators (fromRight')
import qualified Data.ByteString.Lazy.Char8 as BLC
import Data.ByteString.Lazy (toStrict)


data ServiceRequest = ServiceRequest
  { user            :: String
  , computeProvider :: String
  , ntxAmount       :: Integer
  } deriving (Show)

instance FromJSON ServiceRequest where
  parseJSON = withObject "ServiceRequest" $ \v ->
    ServiceRequest <$> v .: "user"
                   <*> v .: "computeProvider"
                   <*> v .: "ntxAmount"

data OracleResponse = OracleResponse
  { resComputeProvider :: String
  , metaDataHash       :: DatumHash
  , withdrawHash       :: DatumHash
  , refundHash         :: DatumHash
  , distributeHash     :: DatumHash
  } deriving (Show)

instance ToJSON OracleResponse where
  toJSON (OracleResponse c m w r di) = object
    [ "resComputeProvider" .= c
    , "metaDataHash" .= show m
    , "withdrawHash" .= show w
    , "refundHash" .= show r
    , "distributeHash" .= show di
    ]

instance FromJSON OracleResponse where
  parseJSON = withObject "OracleResponse" $ \v -> OracleResponse
      <$> v .: "resComputeProvider"
      <*> (stringToDatumHash <$> v .: "metaDataHash")
      <*> (stringToDatumHash <$> v .: "withdrawHash")
      <*> (stringToDatumHash <$> v .: "refundHash")
      <*> (stringToDatumHash <$> v .: "distributeHash")

data RewardRequest = RewardRequest
  { seedPhrase :: String
  , action     :: String
  } deriving (Show)

instance FromJSON RewardRequest where
  parseJSON = withObject "RewardRequest" $ \v ->
    RewardRequest <$> v .: "seedPhrase"
                  <*> v .: "action"


instance ToJSON RewardRequest where
  toJSON (RewardRequest seedPhrase action) = object
    [ "seedPhrase" .= seedPhrase
    , "action"     .= action
    ]



data RewardResponse = RewardResponse
  { sigData        :: Signature
  , sigDataHash    :: DatumHash
  , sigDataDatum   :: BuiltinByteString
  , sigAction      :: Signature
  , sigActionHash  :: DatumHash
  , sigActionDatum :: BuiltinByteString
  } deriving (Show)


instance ToJSON RewardResponse where
  toJSON (RewardResponse sigData sigDataHash sigDataDatum sigAction sigActionHash sigActionDatum) = object
    [ "sigData" .= show sigData
    , "sigDataHash" .= show sigDataHash
    , "sigDataDatum" .= show sigDataDatum
    , "sigAction" .= show sigAction
    , "sigActionHash" .= show sigActionHash
    , "sigActionDatum" .= show sigActionDatum
    ]


data RewardPayload = RewardPayload
  { rewardRequest  :: RewardRequest
  , oracleResponse :: OracleResponse
  } deriving (Show)

instance FromJSON RewardPayload where
  parseJSON = withObject "RewardPayload" $ \v ->
    RewardPayload <$> v .: "rewardRequest"
                  <*> v .: "oracleResponse"

randomString :: Int -> IO String
randomString len = replicateM len $ randomRIO ('a', 'z')

stringToDatumHash :: String -> DatumHash
stringToDatumHash ss= fromString ss :: DatumHash

extractSignature :: SignedMessage a -> Signature
extractSignature (SignedMessage sig _ _) = sig

extractMessageHash :: SignedMessage a -> DatumHash
extractMessageHash (SignedMessage _ hash _) = hash

extractDatum :: SignedMessage a -> BuiltinByteString
extractDatum (SignedMessage _ _ dat) = getDatum $ dat

processServiceRequest :: ServiceRequest -> IO OracleResponse
processServiceRequest (ServiceRequest user computeProvider ntxAmount) = do
  let ntxAmountString = stringToBuiltinByteString $ show $ ntxAmount
      userString = stringToBuiltinByteString user
      computeProviderString =  stringToBuiltinByteString computeProvider
      metaDataString = PlutusTx.Builtins.encodeUtf8  (appendString (appendString  (PlutusTx.Builtins.decodeUtf8  userString) (PlutusTx.Builtins.decodeUtf8 computeProviderString)) (PlutusTx.Builtins.decodeUtf8 ntxAmountString))
      metaDataHash = DatumHash $  metaDataString
  randomStr1 <- randomString 10
  randomStr2 <- randomString 10
  randomStr3 <- randomString 10
  let randomStr1Builtin = stringToBuiltinByteString randomStr1
      randomStr2Builtin = stringToBuiltinByteString randomStr2
      randomStr3Builtin = stringToBuiltinByteString randomStr3
  let withdrawHash = DatumHash $  randomStr1Builtin
      refundHash = DatumHash $   randomStr2Builtin
      distributeHash = DatumHash $  randomStr3Builtin
  return $ OracleResponse computeProvider metaDataHash withdrawHash refundHash distributeHash

processRewardRequest :: RewardRequest -> OracleResponse -> IO RewardResponse
processRewardRequest (RewardRequest seedPhrase action) (OracleResponse _ metaDataHash withdrawHash refundHash distributeHash) = do
  let wallet = generateWallet seedPhrase
      privateKey = extractPrivateKey wallet
      -- Choose the action based on the input parameter
      actionHash = case action of
        "withdraw" -> withdrawHash
        "refund" -> refundHash
        "distribute" -> distributeHash
        _ -> Prelude.error "Invalid action"
      -- Sign the messages
      signedMessageDatum = signMessage' metaDataHash privateKey
      signedMessageAction = signMessage' actionHash privateKey
      sigData = extractSignature signedMessageDatum
      sigAction = extractSignature signedMessageAction
      sigDataDatum = extractDatum signedMessageDatum
      sigActionDatum = extractDatum signedMessageAction
      metaDataMessageHash = extractMessageHash signedMessageDatum
      actionMessageHash = extractMessageHash signedMessageAction
  return $ RewardResponse sigData metaDataMessageHash sigDataDatum sigAction actionMessageHash sigActionDatum



main :: IO ()
main = scotty 3877 $ do
  
  
  post "/request_service" $ do
    requestBody <- body
    
    case decode requestBody :: Maybe ServiceRequest of
      Just serviceRequest -> do
        oracleResponse <- liftIO $ processServiceRequest serviceRequest
        json oracleResponse
      Nothing -> do
        status badRequest400
        liftIO $ runStdoutLoggingT $ logInfoN $ fromRight' . decodeUtf8' . toStrict $  requestBody
        text "Invalid request payload"
        
  post "/request_reward" $ do
    requestBody <- body
    case eitherDecode requestBody :: Either String RewardPayload of
      Right payload -> do
        let req = rewardRequest payload 
        let resp = oracleResponse payload
        liftIO $ runStdoutLoggingT $ logInfoN $ fromRight' . decodeUtf8' . toStrict $ encode req
        liftIO $ runStdoutLoggingT $ logInfoN $ fromRight' . decodeUtf8' . toStrict $ encode resp
        rewardResponse <- liftIO $ processRewardRequest req resp
        json rewardResponse
    
      Left err -> do
        liftIO $ runStdoutLoggingT $ logInfoN $ T.pack err
        status badRequest400
        text "Invalid request payload"

cardano-cli transaction build \
    --alonzo-era \
    --testnet-magic 1 \
    --change-address $(cat ~/workspace/repo/testnet/config/oracle.addr) \
    --tx-in 61965a58a1a899a8cae6420f78c7af89279c37b95e187edbae28caf98884a378#0 \
    --tx-out "addr_test1qqdnrxyyh65x63g9wr89dnlktwep0zj2gde6v6x038vuw4r8crr004x0g0daq06j9hr9j5cxtg8qreh6d8ntuytc3sesh4uk8y 9700000000 lovelace" \
    --out-file  ~/workspace/repo/testnet/tmp/tx.body

cardano-cli transaction sign \
    --tx-body-file ~/workspace/repo/testnet/tmp/tx.body \
    --signing-key-file ~/workspace/repo/testnet/config/oracle.skey \
    --testnet-magic 2 \
    --out-file ~/workspace/repo/testnet/tmp/tx.signed

cardano-cli transaction submit \
    --testnet-magic 1 \
    --tx-file ~/workspace/repo/testnet/tmp/tx.signed

cardano-cli transaction build \
    --alonzo-era \
    --testnet-magic 1 \
    --change-address $(cat  ~/workspace/repo/testnet/config/userAddress.addr) \
    --tx-in edf119f4529a121140913fc234c62e9e62ee405b1cfb0d724f531db37b8ffe52#1 \
    --tx-in-collateral edf119f4529a121140913fc234c62e9e62ee405b1cfb0d724f531db37b8ffe52#1 \
    --tx-in-script-file  ~/workspace/repo/testnet/simple-escrow.plutus \
    --tx-in-datum-file ~/workspace/repo/output/grab-datum.json \
    --tx-in-redeemer-file ~/workspace/repo/output/unit.json \
    --required-signer-hash 8aa9296436db8b3bc49514c7994b960aad28bd9ff745f992556d1e5e \
    --invalid-before 48866954 \
    --protocol-params-file  ~/workspace/repo/testnet/config/protocol.json \
    --out-file tx.body

cardano-cli transaction sign \
    --tx-body-file tx.body \
    --signing-key-file  ~/workspace/repo/testnet/config/userAddress.skey \
    --testnet-magic 1 \
    --out-file tx.signed

cardano-cli transaction submit \
    --testnet-magic 1 \
    --tx-file tx.signed
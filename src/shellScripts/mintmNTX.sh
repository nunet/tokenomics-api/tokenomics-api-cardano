
POLICYID=b8bf52648568e6042bedf6632765a9de2b6fd98779535ca81df62cc5
TOKENNAME=6d4e5458
POLICYSCRIPT=./testnet/config/mNTXPolicy.script
MINTER=addr_test1vq063m2ftxdf4ea6yueg8v4h4tdgvj7hu3dvpllvxd9y36qq5l8ad
MINTERKEY=./testnet/config/oracle.skey

cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1 \
--tx-in 863206dc5a434017e87f26bb77e7edef70f1b04f56bcd65c9171559b6869c535#0 \
--tx-out $MINTER+"1344720 + 10000000 $POLICYID.$TOKENNAME" \
--mint "10000000 $POLICYID.$TOKENNAME" \
--mint-script-file $POLICYSCRIPT \
--change-address $MINTER \
--protocol-params-file testnet/config/protocol.json \
--out-file mint-native-assets.raw

cardano-cli transaction sign \
--signing-key-file $MINTERKEY \
--testnet-magic 1 \
--tx-body-file mint-native-assets.raw \
--out-file mint-native-assets.signed

cardano-cli transaction submit \
--tx-file mint-native-assets.signed \
--testnet-magic 1
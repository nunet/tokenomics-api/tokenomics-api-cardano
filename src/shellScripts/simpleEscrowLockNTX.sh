cardano-cli transaction build \
    --alonzo-era \
    --testnet-magic 1 \
    --change-address addr_test1vq063m2ftxdf4ea6yueg8v4h4tdgvj7hu3dvpllvxd9y36qq5l8ad \
    --tx-in b75245aff75c1b085f3caed57e6d611fea381c182a078e773bc47c99c27c798f#1 \
    --tx-in b767444aa13e9d405da32fde9ee265cb7e3d326f39286b2d3209e5c5ad859237#1 \
    --tx-out "addr_test1qrnjh98ursv2zrfvqwme3x65hksqe2sw3jl3wp45rdm72ewq6um3d3q8smr2en9ydc385cr75s4cav75af7lk8232gxqu83tz4+ 1831199 + 1000000 eb2981df04ea2954aca086449a8a742edf25a02b239bd464669791cb.6d4e5958" \
    --tx-out-datum-hash-file ./output/datum.json \
    --out-file tx.body \
    --protocol-params-file testnet/config/protocol.json 

cardano-cli transaction sign \
    --tx-body-file tx.body \
    --signing-key-file ./testnet/config/oracle.skey \
    --testnet-magic 1 \
    --out-file tx.signed

cardano-cli transaction submit \
    --testnet-magic 1 \
    --tx-file tx.signed
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module DeploySimpleEscrow where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..))
import Codec.Serialise (serialise)
import Data.ByteString.Lazy qualified as LBS
import Data.ByteString.Short qualified as SBS
import Ledger qualified
import Plutus.V1.Ledger.Api as Api
import SimpleEscrow

writeValidator :: FilePath -> Api.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Api.unValidatorScript

writeBountyEscrowScript :: IO (Either (FileError ()) ())
writeBountyEscrowScript =
  writeValidator "testnet/simple-escrow.plutus" $
    SimpleEscrow.validator $
      EscrowParam
        { ntxTokenPolicyId = "8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e56313",
          ntxTokenName = "mNTX",
          oracle = Ledger.PaymentPubKey "c626e0697095e827c0d500d7d62684daca8161537f68caa87218bc52c996cbc8"
        }

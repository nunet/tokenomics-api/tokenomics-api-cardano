const CSL = require("@emurgo/cardano-serialization-lib-nodejs");

const sAddr = "addr_test1vzednh7z9chwujq6dhhjet9dtrjktk4zf68tjvu6g660xkswjrclh";
const addr = CSL.Address.from_bech32(sAddr);
const addrWStakeCredentials =
  CSL.BaseAddress.from_address(addr) ||
  CSL.EnterpriseAddress.from_address(addr);
const pStakeCredential = addrWStakeCredentials.payment_cred();
const pkh = pStakeCredential.to_keyhash();
const pkhHex = pkh.to_hex();
console.log(pkhHex);

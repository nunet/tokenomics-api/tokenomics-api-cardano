{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

module CompileRedeemer where

import            Data.Aeson                as Json   (encode)
import            Data.ByteString.Lazy      qualified as LB
import            System.Environment        (getArgs)
import            Prelude                   hiding (id)
import            Data.String               (fromString)
import qualified PlutusTx

import            Cardano.Api               (scriptDataToJson, ScriptDataJsonSchema(ScriptDataJsonDetailedSchema))
import            Cardano.Api.Shelley       (fromPlutusData)
import            Ledger                    hiding (singleton)
import qualified Ledger


import           SimpleEscrow 


-- This module is here to convert Haskell Data Types to JSON files, particularly used for EscrowRedeemer custom Redeemer Type.
-- To use this enter `cabal run ComileRedeemer 
-- You can use ( /testnet ) ./getPubKeyHash <wallet name> to get your wallet PubKeyHash

-- Constructs the JSON file for the datum, used as input to --tx-in-datum-file in cardano-cli

-- data GiveParams = GiveParams
--     { gpBeneficiary       :: !PaymentPubKeyHash
--     , gpOracle            :: !PaymentPubKeyHash
--     , gpDeadline          :: !POSIXTime
--     , gpRedeemActionToken :: !BuiltinByteString
--     , gpAmount            :: !Integer
--     , gptokenPolicyId     :: !CurrencySymbol
--     , gptokenName         :: !TokenName
--     , gpNTXTokenAmount    :: !Integer
--     } deriving (Generic, ToJSON, FromJSON)


main :: IO ()
main = do
 
  escrowInstanceFund      = SimpleEscrow.EscrowRedeemer Fund
  escrowInstanceFund      = SimpleEscrow.EscrowRedeemer Withdraw
  escrowInstanceFund      = SimpleEscrow.EscrowRedeemer Dispute
  escrowInstanceFund      = SimpleEscrow.EscrowRedeemer Refund
  escrowInstanceFund      = SimpleEscrow.EscrowRedeemer Distribute

  

  writeData ("output/fund-redeemer.json") escrowInstance
  putStrLn "Done"

-- Datum also needs to be passed when sending the token to the script (aka putting for sale)
-- When doing this, the datum needs to be hashed, use cardano-cli transaction hash-script-data ...

writeData :: PlutusTx.ToData a => FilePath -> a -> IO ()
writeData file isData = do
  print file
  LB.writeFile file (toJsonString isData)

toJsonString :: PlutusTx.ToData a => a -> LB.ByteString
toJsonString =
  Json.encode
    . scriptDataToJson ScriptDataJsonDetailedSchema
    . fromPlutusData
    . PlutusTx.toData



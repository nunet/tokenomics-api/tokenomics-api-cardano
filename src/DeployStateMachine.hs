-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE TypeApplications  #-}

-- module DeployStateMachine
--     ( writeJSON
--     , writeValidator
--     , writeUnit
--     , writeEscrowValidator
--     ) where

-- import           Cardano.Api
-- import           Cardano.Api.Shelley   (PlutusScript (..))
-- import           Codec.Serialise       (serialise)
-- import           Data.Aeson            (encode)
-- import qualified Data.ByteString.Lazy  as LBS
-- import qualified Data.ByteString.Short as SBS
-- import           PlutusTx              (Data (..))
-- import qualified PlutusTx
-- import qualified Ledger
-- import           Ledger.Value
-- import           Plutus.Contract.StateMachine

-- import Plutus.V1.Ledger.Api as Api




-- import           StateMachineEscrow

-- dataToScriptData :: Data -> ScriptData
-- dataToScriptData (Constr n xs) = ScriptDataConstructor n $ dataToScriptData <$> xs
-- dataToScriptData (Map xs)      = ScriptDataMap [(dataToScriptData x, dataToScriptData y) | (x, y) <- xs]
-- dataToScriptData (List xs)     = ScriptDataList $ dataToScriptData <$> xs
-- dataToScriptData (I n)         = ScriptDataNumber n
-- dataToScriptData (B bs)        = ScriptDataBytes bs

-- writeJSON :: PlutusTx.ToData a => FilePath -> a -> IO ()
-- writeJSON file = LBS.writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . dataToScriptData . PlutusTx.toData

-- writeValidator :: FilePath -> Api.Validator -> IO (Either (FileError ()) ())
-- writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Api.unValidatorScript

-- writeUnit :: IO ()
-- writeUnit = writeJSON "testnet/unit.json" ()

-- currency :: Ledger.CurrencySymbol
-- currency = "eb2981df04ea2954aca086449a8a742edf25a02b239bd464669791cb"

-- name :: Ledger.TokenName
-- name = "mNTX"


-- token :: Ledger.AssetClass
-- token = AssetClass (currency, name)




-- -- identifer :: ThreadToken
-- -- identifer =  getThreadToken



-- -- writeEscrowValidator :: IO (Either (FileError ()) ())
-- -- writeEscrowValidator = writeValidator "testnet/escrowModule.plutus" $ StateMachineEscrow.escrowValidator $ StateMachineEscrow.EscrowMetaData
-- --     {
-- --       oracle = Ledger.PaymentPubKeyHash "957aaaf3b663749e6fa6d57137bddf03acb5206320a84c17df12c7e4"
-- --     , ntx    = token
-- --     , utxoIdentifer =  identifer
-- --     }
   
module Wallet where

import PlutusTx.Builtins as Builtins
import PlutusTx.Builtins.Class (stringToBuiltinByteString)
import qualified Data.ByteString.Char8 as BSC
import Ledger.Address (PaymentPrivateKey (unPaymentPrivateKey), PaymentPubKey (PaymentPubKey))
import Ledger.Crypto 
import Ledger.CardanoWallet


signRedeemActionToken ::  BuiltinByteString -> PrivateKey -> Signature
signRedeemActionToken  message privateKey = sign' message privateKey

stringToStrictByteString :: String -> BSC.ByteString
stringToStrictByteString = BSC.pack

generateWallet :: String-> MockWallet
generateWallet x = fromSeed' (stringToStrictByteString x)

extractPrivateKey :: MockWallet ->PrivateKey
extractPrivateKey w = unPaymentPrivateKey $ paymentPrivateKey $ w 
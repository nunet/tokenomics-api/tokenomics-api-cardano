{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}

module CompileDatum where

import            Data.Aeson                as Json   (encode)
import            Data.ByteString.Lazy      qualified as LB
import            System.Environment        (getArgs)
import            Prelude                   hiding (id)
import            Data.String               (fromString)
import qualified PlutusTx

import            Cardano.Api               (scriptDataToJson, ScriptDataJsonSchema(ScriptDataJsonDetailedSchema))
import            Cardano.Api.Shelley       (fromPlutusData)
import            Ledger                    hiding (singleton)
import qualified Ledger


import           SimpleEscrow 


-- This module is here to convert Haskell Data Types to JSON files, particularly used for BountyEscrowDatum custom Datum Type.
-- To use this enter `cabal run ComileDatum <price> <seller> <id>`. (price in lovelace, seller as PubKeyHash)
-- You can use ( /testnet ) ./getPubKeyHash <wallet name> to get your wallet PubKeyHash

-- Constructs the JSON file for the datum, used as input to --tx-in-datum-file in cardano-cli

-- data GiveParams = GiveParams
--     { gpBeneficiary       :: !PaymentPubKeyHash
--     , gpOracle            :: !PaymentPubKeyHash
--     , gpDeadline          :: !POSIXTime
--     , gpRedeemActionToken :: !BuiltinByteString
--     , gpAmount            :: !Integer
--     , gptokenPolicyId     :: !CurrencySymbol
--     , gptokenName         :: !TokenName
--     , gpNTXTokenAmount    :: !Integer
--     } deriving (Generic, ToJSON, FromJSON)


main :: IO ()
main = do
  let gbUserAddress       =  Ledger.PaymentPubKeyHash "1fa8ed49599a9ae7ba273283b2b7aada864bd7e45ac0ffec334a48e8" -- fromString  gpBeneficiary'
      gbOracle            =  Ledger.PaymentPubKeyHash "1fa8ed49599a9ae7ba273283b2b7aada864bd7e45ac0ffec334a48e8"  -- fromString  gpOracle'
      gbDeadline          =  POSIXTime $ 1668775946000
      gbRedeemToken =  True --fromString gpRedeemActionToken'
      -- gpAmount            =  2000 --read gpAmount':t
      gbtokenPolicyId     =  "eb2981df04ea2954aca086449a8a742edf25a02b239bd464669791cb" -- fromString gptokenPolicyId'
      gbtokenName         =  "6d4e5958" -- fromString gptokenName'
      -- gpNTXTokenAmount    = 20000--read gpNTXTokenAmount'
      escrowInstance      = SimpleEscrow.GrabParams gbtokenPolicyId gbtokenName gbOracle gbUserAddress gbDeadline gbRedeemToken
  writeData ("output/grab-datum.json") escrowInstance
  putStrLn "Done"

-- Datum also needs to be passed when sending the token to the script (aka putting for sale)
-- When doing this, the datum needs to be hashed, use cardano-cli transaction hash-script-data ...

writeData :: PlutusTx.ToData a => FilePath -> a -> IO ()
writeData file isData = do
  print file
  LB.writeFile file (toJsonString isData)

toJsonString :: PlutusTx.ToData a => a -> LB.ByteString
toJsonString =
  Json.encode
    . scriptDataToJson ScriptDataJsonDetailedSchema
    . fromPlutusData
    . PlutusTx.toData




# This has been taken from PPBL Course Section 201.3 Of Gimbal Labs: Minting Native Assets with `cardano-cli`

we will build a minting transaction using `cardano-cli`.

## Creating a Simple Native Script


1. Create a new directory named `native-scripts`:

```
mkdir native-scripts
cd native-scripts
```

2. Then, create a new file in that directory called `my-first-policy.script`:

```
touch my-first-policy.script
```

3. Open this file and paste the following inside of it:

```
{
    "keyHash": "22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3",
    "type": "sig"
}
```

This is a Native Script with one rule. `"type": "sig"` means that the rule specifies a signature required by any transaction using the script. The `keyHash` defines _which_ signature is required.

Hopefully you have some questions now! Where does that `keyHash` come from? And "who" does it belong to?

## How to generate a Public Key Hash

You can use `cardano-cli` to generate a `keyHash` by using any Verification Key (aka Public Key).

1. Navigate to the testnet-wallet you created in Module 102.

2. Run the command:

```
cardano-cli address key-hash --payment-verification-key-file payment.vkey
```

3. The output of this command is the Public Key Hash of your Public Key. When you use it in a Native Script, you are writing a rule that your Private Key must sign any transaction that uses that Native Script.

4. Copy and paste this `keyHash` into `my-first-policy.script`, (replacing the `keyHash` that belongs to James) and save the file.

## How to generate a Minting PolicyID:

You can use `cardano-cli` to generate the Policy ID for a brand new native asset. In the same directory as your `my-first-policy.script` file, run this command:

```
cardano-cli transaction policyid --script-file my-first-policy.script
```

Congratulations! You just created a brand new token minting policy that can be used to mint tokens on Cardano!

# That's neat, but how do I actually mint tokens?

To mint a token, you must build a minting transaction. (Make sure that your Cardano Testnet Node is synced.)

This transaction will look similar to the transaction you built in 201.2. We're just going to add a few lines that allow us to mint tokens.

Please follow along with the accompanying video for a walkthrough of this step.

## Set Variables

```
POLICYID=
TXIN=
TOKENNAME=
POLICYSCRIPT=/Users/safi/cardano-node-config/mNTXPolicy.script
```

## Minting Transaction:

```
cardano-cli transaction build \
--babbage-era \
--testnet-magic 2 \
--tx-in $TXIN \
--tx-out $MINTER+"2000000 + 1000000 $POLICYID.$TOKENNAME" \
--mint "1000000 $POLICYID.$TOKENNAME" \
--mint-script-file $POLICYSCRIPT \
--change-address $MINTER \
--protocol-params-file proto.json \
--out-file mint-native-assets.raw

cardano-cli transaction sign \
--signing-key-file $MINTERKEY \
--testnet-magic 2 \
--tx-body-file mint-native-assets.raw \
--out-file mint-native-assets.signed

cardano-cli transaction submit \
--tx-file mint-native-assets.signed \
--testnet-magic 1
```


## Read these official docs:

 Cardano: https://developers.cardano.org/docs/native-tokens/minting/




